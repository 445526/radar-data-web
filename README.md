# radar-data-web

Can server as a template on how to make nginx serve static files from a directory.

## Usage

```bash
./kubectl --kubeconfig=config.yaml apply -f deployment.yaml -n celldeath-ns
```

The app will be served at `https://sentinel1-data.dyn.cloud.e-infra.cz` (see `deployment.yaml`)